using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Cards
{

    public class Defoult_Card : Card
    {
        private CardDisplay _cardDisplay = null;

        public override void OnClicked()
        {
            _cardDisplay.OnEnableText();
            _cardDisplay.UpdateText(_distanceToHider);
        }

        private void Awake()
        {
            _cardDisplay = new CardDisplay(GetComponentInChildren<TMPro.TMP_Text>());
        }
    }
}