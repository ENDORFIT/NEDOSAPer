using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace Cards
{

    public class Bomb_Card : Card
    {
        [SerializeField] private GameObject _explosionPrefab;

        public override void OnClicked()
        {
            Instantiate(_explosionPrefab, transform.position, Quaternion.identity);
            GameManager.Instant()?.GameOver();
            OnDisable();
        }

        private void Awake()
        {
            GameManager.Instant().OnSendGameOver += OnGetMassageGameOver;
        }

        private void OnGetMassageGameOver(object sender, System.EventArgs e)
        {
            StartCoroutine(nameof(Explosion));
        }

        private IEnumerator Explosion()
        {
            yield return new WaitForSeconds(_timeDelay);
            InstantiateExplosiveObject();
        }

        private void InstantiateExplosiveObject()
        {
            Instantiate(_explosionPrefab, transform.position, Quaternion.identity);
            OnDisable();
        }

        private void OnDisable()
        {
            GameManager.Instant().OnSendGameOver -= OnGetMassageGameOver;
        }
    }
}
