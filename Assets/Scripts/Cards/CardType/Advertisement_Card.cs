using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Cards
{
    public class Advertisement_Card : Card
    {
        private Canvas _canvas = null;
        public override void OnClicked()
        {
            _canvas.gameObject.SetActive(true);
        }

        private void Awake()
        {
            _canvas = GetComponentInChildren<Canvas>();
            _canvas.gameObject.SetActive(false);
        }
    }
}