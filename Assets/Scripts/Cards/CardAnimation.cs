using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Cards
{
    public class CardAnimation
    {
        private string _explosiveNameAnimation = "Esplosive";
        private Animator _animator = null;

        private string _flipCardNameAnimation = "FlipCard";

        public CardAnimation(Animator ctxAnimator)
        {
            _animator = ctxAnimator;
        }

        public void ExplosiveAnimation()
        {
            _animator.SetBool(_explosiveNameAnimation, true);
        }
        public void FlipCard()
        {
            _animator.SetBool(_flipCardNameAnimation, true);
        }
    }
}