using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cards
{
    [CreateAssetMenu(fileName = "Cards menu", menuName = "Menu / Cards", order = 1)]
    public class CardsMenu : ScriptableObject
    {
        public GameObject HiderPrefab = null;
        public GameObject DefoultPrefab = null;
        public GameObject BombPrefab = null;
        public GameObject AdvertisementPrefab = null;
        public GameObject GoldenPrefab = null;
    }
}