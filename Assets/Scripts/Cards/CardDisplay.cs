using UnityEngine;
using TMPro;

namespace Cards
{
    public class CardDisplay
    {
        private TMP_Text _text = null;

        public CardDisplay(TMP_Text ctxText)
        {
            _text = ctxText;
            OnDisableText();
        }

        public void UpdateText(int _distanceToHider)
        {
            _text.text = "" + _distanceToHider;
        }

        public void OnEnableText()
        {
            _text.gameObject.SetActive(true);
        }

        public void OnDisableText()
        {
            _text.gameObject.SetActive(false);
        }
    }
}