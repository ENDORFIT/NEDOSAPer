using UnityEngine;


namespace Cards
{
    public static class CardsFunction
    {
        public static CardType RandomCardType(int ctx)
        {
            if (ctx == 0) return CardType.Hider;
            else if (Random.Range(0, 10) == 1) return CardType.Bomb;
            else if (Random.Range(0, 100) == 1) return CardType.Golden;
            else if (Random.Range(0, 10) == 1) return CardType.Advertisement;
            else return CardType.Defoult;
        }
    }
}