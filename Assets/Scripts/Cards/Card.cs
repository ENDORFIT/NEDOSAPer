using UnityEngine;

namespace Cards
{
    public abstract class Card : MonoBehaviour
    {
        [Header("Defoult card settings")]
        protected int _distanceToHider = 0;

        [Header("Extended settings")]
        protected float _timeDelay = 1.0f;

        protected CardAnimation _cardAnimation = null;

        public void Initialization(int ctxDistanceToHider = 0)
        {
            _distanceToHider = ctxDistanceToHider;
        }

        public abstract void OnClicked();

        private void Awake()
        {
            _timeDelay = Random.Range(0.0f, 2.0f);
            _cardAnimation = new CardAnimation(GetComponentInChildren<Animator>());
        }
    }
}