using UnityEngine;

public class CheckCardSystem : MonoBehaviour
{
    private HandlerInputSystem _input = null;

    private void Start()
    {
        _input = GetComponent<HandlerInputSystem>();
        _input.OnTouched += OnTouched;
    }

    private void OnTouched(object sender, System.EventArgs e) // ��� ��� ���� xD 
    {
        Vector2 tempVector = Camera.main.ScreenToWorldPoint(_input.Postion());
        RaycastHit2D hit = Physics2D.Raycast(tempVector, tempVector);

        if (hit && hit.transform.gameObject?.GetComponent<Cards.Card>()) hit.transform.gameObject?.GetComponent<Cards.Card>().OnClicked();
    }
}
