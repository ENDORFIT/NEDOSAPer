using System;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;


public class HandlerInputSystem : MonoBehaviour
{
    public event EventHandler OnTouched;

    private Vector2 _position = Vector2.zero;
    public Vector2 Postion() => _position;

    public void GetScreenPostion(InputAction.CallbackContext ctx) 
    { 
        _position = ctx.ReadValue<Vector2>(); 
    }

    public void OnTouch(InputAction.CallbackContext ctx)
    {
        if (ctx.started) OnTouched?.Invoke(this, EventArgs.Empty);
    }   
    
}
