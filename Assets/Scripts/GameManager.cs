using System;
using System.Collections;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public event EventHandler OnSendGameOverUI;
    public event EventHandler OnSendGameOver;

    private static GameManager _instant = null;
    public static GameManager Instant() => _instant;

    private float _timeToSendStartEvent = 3.0f;

    public void GameOver()
    {
        StartCoroutine(nameof(OnSendMassageGameOver));
        OnSendGameOver?.Invoke(this, EventArgs.Empty);
    }

    private IEnumerator OnSendMassageGameOver( )
    {
        yield return new WaitForSeconds(_timeToSendStartEvent);
        OnSendGameOverUI?.Invoke(this, EventArgs.Empty);
    }

    private void Awake()
    {
        DontDestroyOnLoad(this);
        if (!_instant) _instant = this;
        else Destroy(this);
    }
}
