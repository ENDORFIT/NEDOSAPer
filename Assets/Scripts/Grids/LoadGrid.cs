using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Grids
{

    public class LoadGrid : MonoBehaviour
    {
        private Grid _grid = null;

        [SerializeField] private int _width = 10, _height = 10;
        [SerializeField] private Vector2 _offset = new Vector2(1, 1);
        [SerializeField] private Vector2 _startCoordinate = new Vector2(0, 0);

        [SerializeField] private Cards.CardsMenu _cardMenu = null;

        private void Awake()
        {
            _grid = new Grid(_width, _height, _startCoordinate, _offset, _cardMenu);
        }
    }
}