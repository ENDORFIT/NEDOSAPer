using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Grids
{
    public class Grid 
    {
        private int _width, _height;
        private Vector2 _offset;
        private Vector2[,] _coordinates;

        private DrawGrid _drawGrid = null;
        private CreateGrid _createGrid = null;

        public Grid(int ctxWidth, int ctxHeight, Vector2 ctxStartCoordinate, Vector2 ctxOffset, Cards.CardsMenu ctxCardMenu) {
            _width = ctxWidth;
            _height = ctxHeight;
            _offset = ctxOffset;

            _coordinates = new Vector2[ctxHeight, ctxWidth];
            SetCoordinateGrid(ctxStartCoordinate);

            _createGrid = new CreateGrid(this);
            _drawGrid = new DrawGrid(this, ctxCardMenu);
        }

        public int Width() => _width;
        public int Height() => _height;
        public int[,] MatrixCards() => _createGrid.MatrixCards();
        public Vector2[,] Coordinates() => _coordinates;


        private void SetCoordinateGrid(Vector2 ctx)
        {
            for (int y = 0; y < Height(); y++)
            {
                for (int x = 0; x < Width(); x++)
                {
                    _coordinates[y, x] = new Vector2(ctx.x + _offset.x * x, ctx.y + _offset.y * y);
                }
            }
        }
    }
}