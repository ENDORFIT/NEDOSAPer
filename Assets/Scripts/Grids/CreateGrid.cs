using UnityEngine;

namespace Grids
{
    public class CreateGrid : MonoBehaviour
    {
        private Grid _grid = null;

        private int[,] _matrixCards;
        private int _indexMainElement_X, _indexMainElement_Y;

        public CreateGrid(Grid ctxGrid)
        {
            _grid = ctxGrid;
            CreateGridCardsLogic();
        }

        public int[,] MatrixCards() => _matrixCards;

        public void CreateGridCardsLogic()
        {
            _matrixCards = new int[_grid.Height(), _grid.Width()];
            _indexMainElement_X = Random.Range(0, _grid.Width()-1);
            _indexMainElement_Y = Random.Range(0, _grid.Height()-1);

            for (int y = 0; y < _grid.Height(); y++)
            {
                for (int x = 0; x < _grid.Width(); x++)
                { 
                    _matrixCards[y, x] = Mathf.Max(Mathf.Abs(_indexMainElement_Y - y), Mathf.Abs(_indexMainElement_X - x));
                }
            }

        }
    }
}