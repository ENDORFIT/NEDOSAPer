using UnityEngine;

namespace Grids
{
    public class DrawGrid
    {
        private Cards.CardsMenu _cardMenu = null;
        private Grid _grid  = null;

        public DrawGrid (Grid ctxGrid, Cards.CardsMenu ctxCardPrefab)
        {
            _grid = ctxGrid;
            _cardMenu = ctxCardPrefab;
            DrawGrids();
        }

        private void DrawGrids()
        {
            for (int y = 0; y < _grid.Height(); y++)
            {
                for (int x = 0; x < _grid.Width(); x++)
                {
                    Cards.CardType _cardType = Cards.CardsFunction.RandomCardType(_grid.MatrixCards()[y, x]);

                    GameObject card;
                    switch (_cardType)
                    {
                        case Cards.CardType.Hider:
                            card = InstantiateObject(_cardMenu.HiderPrefab, x, y);
                            card.GetComponent<Cards.Card>().Initialization(_grid.MatrixCards()[y, x]);
                            break;
                        case Cards.CardType.Defoult:
                            card = InstantiateObject(_cardMenu.DefoultPrefab, x, y);
                            card.GetComponent<Cards.Card>().Initialization(_grid.MatrixCards()[y, x]);
                            break;
                        case Cards.CardType.Bomb:
                            InstantiateObject(_cardMenu.BombPrefab, x, y);
                            break;
                        case Cards.CardType.Advertisement:
                            InstantiateObject(_cardMenu.AdvertisementPrefab, x, y);
                            break;
                        case Cards.CardType.Golden:
                            InstantiateObject(_cardMenu.GoldenPrefab, x, y);
                            break;
                    }
                } 
            }
        }
        private GameObject InstantiateObject(GameObject _cardType, int x, int y)
        {
            GameObject ctx = GameObject.Instantiate(_cardType);
            ctx.transform.position = new Vector3(_grid.Coordinates()[y, x].x, _grid.Coordinates()[y, x].y, 0);
            return ctx;
        }
    }
}